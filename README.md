# Magic config generator 

This is an experiment with the OpenAI ChatGPT api to create common configuration files for things like 
* Docker compose files
* ESLint configurations
* Vite configurations

# Setup 
* You'll need an API key and add that to your system's environmental variables under the key `OPENAI_KEY`.
* The rest of the app is a fairly standard Next.js app with just one route. 
* Run `npm run dev` to start the development server.
