export const config_types = [

    {

        name:"docker-compose",

        options:[

            "LAMP",

            "MAMP",

            "PHP",

            "NodeJS"

        ]

    },

    {

        name:"eslint",

        options:[

            "React",

            "Vue",

            "Svelte",

            "Rollup",

            "Webpack",

            "Meteor"

        ]

    },

    {

        name:"vite.config.js",

        options:[

            "React",

            "Vue",

            "Svelte",

            "Rollup",

            "Webpack",

            "Meteor"

        ]

    }

]

