'use client'

import styles from './page.module.css'
import {ChangeEvent, useEffect, useRef, useState} from "react";
import {config_types} from "@/app/lib/filetypes";
import ReactCodeMirror from "@uiw/react-codemirror";
import {javascript} from "@codemirror/lang-javascript"
import {langs} from "@uiw/codemirror-extensions-langs";

interface CompleteResponse {
    text: string
    index: number
    finish_reason: string
}


export default function Home() {

    // whether or not there's an error message
    const [errorMessage, setErrorMessage] = useState("")

    const [currentConfigIndex, setConfigIndex] = useState(0)

    const [baseOptions, setBaseOptions] = useState()
    const [subOptions, setSubOptions] = useState([])

    // holds the output once it comes in.
    const [configFile, setConfigFile] = useState("")

    // holds the message from the api
    const [outputMsg, setOutputMsg] = useState("")

    const [isWorking, setIsWorking] = useState(false)

    const base_type = useRef<HTMLSelectElement>(null)
    const sub_type = useRef<HTMLSelectElement>(null)
    const additional_options = useRef<HTMLTextAreaElement>(null)

    useEffect(() => {


        ////// SET BASE OPTIONS //////////

        let baseOpts: Array<JSX.Element> = [
            (<option key={0} value={"null"}>Select One</option>)
        ]

        for (let type in config_types) {
            let t = config_types[type]
            baseOpts.push(
                (
                    <option key={baseOpts.length} value={t.name}>{t.name}</option>
                )
            )
        }

        //@ts-ignore
        setBaseOptions(baseOpts)

        ////// SET SUB OPTIONS //////////

        let allOpts = []

        for (let type in config_types) {
            let t = config_types[type]
            let options = t.options
            let subOpts: Array<JSX.Element> = []

            for (let opt in options) {
                let subopt = options[opt]

                subOpts.push(
                    (
                        <option key={subOpts.length} value={subopt}>{subopt}</option>
                    )
                )
            }

            allOpts.push(subOpts)

        }


        //@ts-ignore
        setSubOptions(allOpts)

    }, [])


    /**
     * Event handler when config type is selected.
     * @param e
     */
    function on_config_change(e?: ChangeEvent<HTMLSelectElement>) {

        let target = e?.target as HTMLSelectElement
        setConfigIndex(target.selectedIndex)

    }

    /**
     * Submit settings to API
     */
    function submit() {
        let type = base_type!.current!.value
        let subtype = sub_type!.current!.value
        let additional = additional_options!.current!.value

        const base_url = `/api/ai?base_type=${type}&sub_type=${subtype}&additional=${additional}`

        if (type !== null && subtype !== "" && !isWorking) {

            // set flag so we don't submit multiple requests
            setIsWorking(true)

            // reset config file
            setConfigFile("")

            fetch(base_url).then(res => res.json()).then(data => {
                parseResponse(data)
            })
        }

    }

    function parseResponse(e: Array<CompleteResponse>) {

        buildFile(e[0].text)
        setIsWorking(false)
    }


    /**
     * Parses the response and builds out the content for the config file to show the user
     * @param text {string} the text string returned by the OpenAI api.
     */
    function buildFile(text: string) {

        // split content based on newlines
        let split = text.split("\n")

        // holds index with the message response before the actual file content
        let msg_index = 0

        // extract the message index
        for (let i = 0; i < split.length; ++i) {
            if (split[i] !== "") {
                msg_index = i
                break;
            }
        }

        // build the message
        // TODO sometimes there's no "Here a blah blah" file so for now assume no such message will appear.
        let msg = split.splice(msg_index, 1)[0]

        // build the file
        let file = split.join("\n\n")

        //setOutputMsg(msg)
        setConfigFile(msg + file)
    }

    function download() {
        let element = document.createElement("a")
        element.setAttribute("href", `data:text/plain;charset=utf-8, ${configFile}`)
        element.setAttribute("download", "file.js")
        element.style.display = "none"
        document.body.appendChild(element)
        element.click()
        document.body.removeChild(element)
    }

    return (

        <>
            <section className={"py-20"}>
                <h1 className={`${styles.pageTitle} text-center`}>Magic Config Generator</h1>
                <p className={"text-center text-2xl"}>
                    A tool to help quickly whip up a config file for things like Docker, ESLint and other common stacks
                    that
                    that can operate with configuration files.
                </p>
            </section>

            <section>
                <div className={styles.config_type_option}>
                    <p>I would like to create a </p>

                    <select ref={base_type} className={styles.select} onChange={on_config_change}>
                        {baseOptions}
                    </select>
                    <p> file</p>
                </div>

                <div className={styles.config_type_option} style={{display: currentConfigIndex > 0 ? 'flex' : 'none'}}>
                    <p>that contains the base options for a </p>

                    <select ref={sub_type} className={styles.select}>
                        {subOptions[currentConfigIndex - 1]}
                    </select>
                    <p> stack.</p>
                </div>

                <div className={styles.config_type_option} style={{flexDirection: "column"}}>
                    <p className="py-5">Additional instructions:</p>
                    <textarea ref={additional_options} className={styles.textarea}></textarea>
                </div>


                <div className={styles.config_type_option}>
                    <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                            onClick={submit}>Submit
                    </button>
                </div>


                <div className={styles.config_type_option} style={{display: isWorking ? 'flex' : 'none'}}>
                    <p>Loading</p>
                </div>

                <div className={styles.config_type_option} style={{display: configFile != "" ? 'flex' : 'none'}}>
                    <div className="py-5">
                        <p className="py-5">{outputMsg}</p>
                        <ReactCodeMirror
                            value={configFile}
                            height={"500px"}
                            extensions={[
                                javascript({jsx: true}),
                                langs.dockerfile()
                            ]}/>

                        {/*
                        TODO file currently doesn't preserve newlines so not worrying about this for now.
                         <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                onClick={download}>Download
                        </button>
                        */}
                    </div>
                </div>

            </section>
        </>
    )
}
