import dotenv from "dotenv"
import {NextApiRequest, NextApiResponse} from "next";
import * as process from "process";

// Connect to OpenAI service.
// Note that this requires an environmental variable called "OPENAI_KEY" in your
// environmental variables

dotenv.config()

interface CompleteResponse {
    text:string
    index:number
    finish_reason:string
}
type Data = {
    choices:Array<CompleteResponse>
}

// TODO maybe make this setable
const settings = {
    tmp: 0.7,
    max_tokens: 264,
    model: "text-davinci-003",
    top_p: 1,
    content_type: 'application/json',
    authorization: `Bearer ${process.env["OPENAI_KEY"]}`,
    endpoint: "https://api.openai.com/v1/completions"
}

export default function handler(
    req: NextApiRequest,
    res: NextApiResponse<Data>
) {


    let request = {
        temperature: settings.tmp,
        max_tokens: settings.max_tokens,
        model: settings.model,
        top_p: settings.top_p,
        prompt: ""
    }

    let type = req.query.base_type
    let sub_type = req.query.sub_type
    let additional_options = req.query.additional

    let query = `can you give me a basic ${type} file for a ${sub_type} setup?`

    query += `Additional instructions: ${additional_options}`

    request.prompt = query

    console.log("Fetching based on prompt - ", `"${query}"`)
    fetch(settings.endpoint, {
        method: 'POST',
        headers: {
            'Content-type': settings.content_type,
            'Authorization': settings.authorization
        },
        body: JSON.stringify(request)
    }).then(res => res.json())
        .then(data => {
            console.log("response success - ", data)
            res.status(200).json(data.choices)
        }).catch(e => {
            console.log("Error occured ", e)
        res.status(200).json(e)
    })

}
